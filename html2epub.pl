#!/usr/bin/perl
#===============================================================================
#
#         FILE: html2epub
#
#        USAGE: html2epub -u http://someurl/ -o outputfile.type
#
#  DESCRIPTION: Converts a htmp page in a epub document using the pandoc converter
#
# REQUIREMENTS: The pandoc utility
#
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Nelo R. Tovar, tovar.nelo@gmail.com
#      COMPANY:  ---
#      VERSION:  ---
#      CREATED:  16/01/2015 
#      REVISION: Date         Autor                Change
#===============================================================================

use strict;
use warnings;
use v5.10.1;
use LWP::Simple;
use Getopt::Long::Descriptive;
use File::Temp qw/ tempfile /;

our $VERSION = '0.1';
$| = 1;
my ( $opt, $usage ) = describe_options(
	"$0 %o ",
	['url|u=s',    "Url to convert (required)",  {required => 1}],
	['output|o=s', "output filename (required)", {required => 1}],
	[],
	['verbose|V', "Show Process Info"],
	['debug|d',   "Show Debug Info"],
	['version|v', "Show The Version"],
	['help|h',    "Show This Help"],
);

our $DEBUG = '';
$DEBUG .= 'V' if $opt->verbose;
$DEBUG .= 'd' if $opt->debug;

#our $USER   = 'nt_iutav@yahoo.com.ve';    # $opt->user() || $ENV{USER};
#our $PASSWD = 'Iutavn3l0.';               #$opt->pass() || undef;

main($opt);

###########################################################

sub main {
	my ($opt) = @_;
	if ( $opt->help ) {
		say( $usage->text );
	} elsif ( $opt->version ) {
		say "$0 Version $VERSION";
	} else {
		process($opt);

		#say( $usage->die( {pre_text => "Error: No se indicó una acción a ejecutar\n"} ) );
	}
}

sub process {

=head2 process


=cut

	my ($opt) = @_;

	my $pandocbin = `which pandoc`;
	die "The pandoc converter tool not found, please install it" unless $pandocbin;
	chomp $pandocbin;

	my $tempfile;
	(undef, $tempfile) = tempfile(TEMPLATE => 'tempXXXX', DIR => '.', SUFFIX => '.html', OPEN => 0);
	my $outputfilename = $opt->output;

	say "tempfile => $tempfile\n outputfile => $outputfilename\n pandoc =>$pandocbin" if debug();

	say 'Download html page' if verbose() or debug();
	my $rc = getstore( $opt->url(), $tempfile );
	if ( is_success($rc) ) {
	    say 'Converting html file' if verbose() or debug();
		say "Executing $pandocbin -f html -o $outputfilename $tempfile" if debug();
		system("$pandocbin -f html -t epub -o $outputfilename $tempfile");

		say "Unlink temporaly file: $tempfile " if debug();
		unlink $tempfile;
	} else {
		die "Error at download of the html page. Error Code: $rc";
	}

	# End of sub proccess
}

sub verbose {

	# Test if verbose option is on
	return $DEBUG =~ /V/;
}

sub debug {

	# Test if DEBUG option is on
	return $DEBUG =~ /d/;
}

__END__
